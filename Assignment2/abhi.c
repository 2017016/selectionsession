#include <stdio.h>
#include <stdlib.h>

int main() {
	int size;// size of array
	scanf("%d",&size);// take input from user i.e. size of the array
	int iterator;//declare variable for iterating
	int* memory; //pointer that hold the base address of the memory block 
	
	//Allocate memory using malloc() dynamically
	memory = (int*)malloc(size*sizeof(int));
	
	if(memory!=NULL) {        //means memory is allocatted 
	    for(iterator=0;iterator<size;++iterator){
	        memory[iterator]=iterator;
	    }
	    printf("array is:");
	    for(iterator=0;iterator<size;++iterator){
	    printf("\n%d",memory[iterator]);
	     
	}

	    return 1;
	}
	else{//means memory is not allocatted
	    return 0;
	}
	
	
}