package pac;
import java.util.*;
import java.util.function.Function;

public class  CaseConverter {

	
	public static String convert(String caseType, String inputString) 
	{
	 String required = 	caseType;
	  Map<String, Function<String,String>>map = new HashMap<String, Function<String,String>>(); 
	  
	  String outputString = "";
	  
	  Function<String,String> changetoProper = proper -> { String proper_String = proper.substring(0,1).toUpperCase()+ proper.substring(1);
	  return proper_String;
	  };
	  
	  Function<String,String> changetoLower = lower -> lower.toUpperCase();  
	  
	  Function<String,String> changetoUpper = upper -> upper.toUpperCase();
	  
	  map.put("upper",changetoUpper);
	  map.put("lower",changetoLower);
	  map.put("proper",changetoProper);
	  
	  if(map.containsKey(required)) {
		  outputString = map.get(required).apply(inputString);
	  }
	  
		return outputString;
	}
	
	public static void main(String[] args) {
		
		Scanner s = new Scanner(System.in);
		String string_type = s.next();
		String str = s.next();
		
	System.out.println(convert(string_type,str));
	}
}
