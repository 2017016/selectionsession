using System;
using System.Threading;

class ImplementsMutliThreading
{
    // Create a new Mutex. The creating thread does not own the mutex.
    private static Mutex mut = new Mutex();
    private const int numIterations=50;
    private const int numThreads; // Number of threads 
    static int static_var = 0;

    static void Main()
    {
        
            Console.Write("Enter integer value: ");
            string userInput = Console.ReadLine();
            numThreads = Convert.ToInt32(userInput); 
            
        // Create the threads that will increment/use static variable (or use the protected resource)
        for(int i = 0; i < numThreads; i++)
        {
            Thread newThread = new Thread(new ThreadStart(ThreadProc));
            newThread.Name = String.Format("Thread{0}", i + 1); // for naming the each thread
            newThread.Start();
        }

        // The main thread exits, but the application continues to run until all foreground threads have exited.
    }

    private static void ThreadProc()
    {
        for(int i = 0; i < numIterations; i++)
        {
            UseStaticVariable();
        }
    }

    // This method represents a resource that must be synchronized
    // so that only one thread at a time can enter and modify variable.
    private static void UseStaticVariable()
    {
        // Wait until it is safe to enter.
        mut.WaitOne();

        // code to access non-reentrant resources here.
        
         int itr = 0;
         while(itr!=numIterations)
            {
                ImplementsMutliThreading.static_var = ImplementsMutliThreading.static_var + 1;
                Console.WriteLine("{0} is running", Thread.CurrentThread.Name);
                Console.WriteLine("Current value of static variable is {0}", ImplementsMutliThreading.static_var);
                itr++;
            }

        Thread.Sleep(500);

        // Release the Mutex.
        mut.ReleaseMutex();
        
        Console.WriteLine("Final Value (50 multiply by number of threads) is {0}",50*numThreads);
    }
}