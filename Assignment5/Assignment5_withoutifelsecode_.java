package code;
import java.util.*;

public class Abhi {
	
	public static String fun(int day) {
		
		String dayName = "Invalid";
		
		
		HashMap<Integer, String> map = new HashMap<>();
		
		map.put(1, "Monday"); 
		map.put(2, "Tuesday"); 
		map.put(3, "Wednesday"); 
		map.put(4, "Thursday"); 
		map.put(5, "Friday"); 
		map.put(6, "Saturday"); 
		map.put(7, "Sunday"); 
		
		String result = ((map.get(day) == null) ? dayName : map.get(day));
		return result;
	}
	
	    public static void main(String[] args) 
	    {
	    	Scanner s = new Scanner(System.in);
	    	System.out.println(fun(11));
	    }
}
	    	 


