#include <windows.h>   // windows.h is a Windows-specific header file for the C and C++ programming languages which contains declarations for all of the functions in the Windows API. It defines a very large number of Windows specific functions that can be used in C. windows.h = excpt.h + stdarg.h + string.h etc.

const char ClassName[] = "myWindowClass"; //The variable stores the name of our window class

//WinMain() use to register our window class
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
    WNDCLASSEX wc;
    HWND hwnd;
    MSG Msg;

    //1. Registering the Window Class
	//The members of the struct affect the window class:
    wc.cbSize        = sizeof(WNDCLASSEX);//size of the structure
    wc.style         = 0;
    wc.lpfnWndProc   = WndProc; //Pointer to the window procedure for this window class
    wc.cbClsExtra    = 0; //Amount of extra data allocated for this class in memory. Usually 0
    wc.cbWndExtra    = 0; //Amount of extra data allocated in memory per window of this type. Usually 0
    wc.hInstance     = hInstance; //Handle to application instance (that we got in the first parameter of WinMain())
    wc.hIcon         = LoadIcon(NULL, IDI_APPLICATION); //Large (usually 32x32) icon shown when the user presses Alt+Tab.
    wc.hCursor       = LoadCursor(NULL, IDC_ARROW);//Cursor that will be displayed over our window
    wc.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);//Background Brush to set the color of our window
    wc.lpszMenuName  = NULL;//Name of a menu resource to use for the windows with this class
    wc.lpszClassName = ClassName; //ame to identify the class with
    wc.hIconSm       = LoadIcon(NULL, IDI_APPLICATION); //Small icon to show in the taskbar and in the top left corner of the window.


//RegisterClassEx() and check for failure, if it fails we pop up a message which says so and abort the program by returning from the WinMain() function.
    if(!RegisterClassEx(&wc))
    {
        MessageBox(NULL, "Window Registration Failed!", "Error!",
            MB_ICONEXCLAMATION | MB_OK);
        return 0;
    }

    //2. Creating the Window
    hwnd = CreateWindowEx(WS_EX_CLIENTEDGE,ClassName,"The title of my window",WS_OVERLAPPEDWINDOW,CW_USEDEFAULT, CW_USEDEFAULT, 240, 120,NULL, NULL, hInstance, NULL);

    if(hwnd == NULL)
    {
        MessageBox(NULL, "Window Creation Failed!", "Error!",
            MB_ICONEXCLAMATION | MB_OK);
        return 0;
    }

    ShowWindow(hwnd, nCmdShow);
    UpdateWindow(hwnd);

    // 3. The Message Loop
	//GetMessage() gets a message from your application's message queue. By calling GetMessage() you are requesting the next available message to be removed from the queue and returned to you for processing
    while(GetMessage(&Msg, NULL, 0, 0) > 0) 
    {
        TranslateMessage(&Msg); //TranslateMessage() does some additional processing on keyboard events like generating WM_CHAR messages to go along with WM_KEYDOWN messages
        DispatchMessage(&Msg); //Finally DispatchMessage() sends the message out to the window that the message was sent to 
    }
    return Msg.wParam;
}

//4. the Window Procedure
//This is where all the messages that are sent to our window get processed
LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    switch(msg)
    {
        case WM_CLOSE:
            DestroyWindow(hwnd);
        break;
        case WM_DESTROY:
            PostQuitMessage(0);
        break;
        default:
            return DefWindowProc(hwnd, msg, wParam, lParam);
    }
    return 0;
}
