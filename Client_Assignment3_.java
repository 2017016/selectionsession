import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Client {

	public static void main(String[] args) throws UnknownHostException, IOException {
		
		String str,temp; // declare two String variables which will used for sending the message to the client
		
		Scanner sc = new Scanner(System.in); // Scanner class for taking input
		
		Socket s = new Socket("127.0.0.1",1342);// create a new socket and assign IP adress and port number, you can use any port number 
		
		Scanner sc1 = new Scanner(s.getInputStream());//for accepting message from the server
		
		str = sc.next();// for taking input from client
		
		PrintStream p = new PrintStream(s.getOutputStream());// declare print stream object and pass to the server
		
		p.println(str);// print message onto the server using printStream Object
		
		temp = sc1.next();//store the result form the server
		
		System.out.println(temp); // print the message/feedback of the server
	}

}
