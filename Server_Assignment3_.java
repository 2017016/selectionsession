import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Server {

	public static void main(String[] args) throws IOException {
		
		String str,temp;
		
		ServerSocket s1 = new ServerSocket(1342);//create a server socket and specify the same port number as assigned in client side 
		
		Socket ss = s1.accept(); // it accept the incoming request to the socket
		
		Scanner sc = new Scanner(ss.getInputStream());//getInputStream() is used to accept message which client want to pass
		
		str = sc.next();//store message which got from the client
		
		temp = "Hii_from_server";
		
		// passing result back to the client
		PrintStream p = new PrintStream(ss.getOutputStream());
		p.print(temp);// send message to the client
	}

}
